## Postgres Basics


### Create Database

```bash

createdb -E UTF8 -O zhiruchen -T template0 --lc-collate en_US.UTF-8 --lc-ctype en_US.UTF-8 briver

```

```sql

CREATE DATABASE briver WITH TEMPLATE = template0 ENCODING = 'UTF8' \
 LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';

```

* -E 指定 编码
* -O 指定 owner
* -T 指定模板
* --lc-collate [String sort order](https://www.postgresql.org/docs/12/locale.html)

### Create tables

```sql
CREATE TABLE blog (
   id CHAR(27) PRIMARY KEY,
   name VARCHAR(200) NOT NULL DEFAULT '',
   slug VARCHAR(300) NOT NULL DEFAULT '',
   source_id CHAR(27) NOT NULL,
   source_url VARCHAR(500) NOT NULL DEFAULT '',
   description VARCHAR(1000) NOT NULL DEFAULT '',
   view_count integer NOT NULL DEFAULT 0,
   like_count integer NOT NULL DEFAULT 0,
   share_count integer NOT NULL DEFAULT 0,
   created_at timestamp without time zone DEFAULT now() NOT NULL,
   updated_at timestamp without time zone DEFAULT now() NOT NULL
);
```

### Creare Index

```sql
CREATE INDEX idx_name ON table_name(field_name);
```

### Create Unique

```sql
ALTER TABLE table_name ADD CONSTRAINT unique_name UNIQUE (field_name);
```